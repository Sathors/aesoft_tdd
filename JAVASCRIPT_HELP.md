# AYUDA DE NODE Y JAVASCRIPT

Vamos a usar node para este ejercicio.

## Referencia adicional

Para más detalle, por favor ver [esta página](https://learnxinyminutes.com/docs/es-es/javascript-es/) o [esta](http://hyperpolyglot.org/scripting).

## ¿Qué es Node.js?

Node.js es una implementación de Javascript de lado del servidor. Ahora se usa mucho en la industría, permite desarrollar su aplicación usando solamente Javascript (Node.js de lado del servidor y Javascript de lado del cliente, en el navegador).

## Caracteristicas de Javascript

Javascript es un lenguaje de script, así que es *interpretado*, no *compilado*. Es un lenguaje de tipado dinamico, es decir que no se puede declarar el tipo de una variable.
Esto es válido:

```javascript
let miVariable = 3
miVariable = 'hola'
```

Javascript es muy parecido a Python, Ruby o PHP.

## ¿Cómo crear una clase?

Javascript no tiene clases en sí, más bien maneja el concepto de prototipos. Pero para nuestro taller, pueden pensar que son clases mismo.

Definición de una clase:

```javascript
class MiClase {
    constructor (parametro1, parametro2) {
        this.propiedad1 = parametro1
        this.propiedad2 = parametro2
    }

    metodo1 (parametro1, parametro2) {
        return parametro1 + parametro2 + this.propiedad1
    }

    metodo2 (parametro1) {
        return this.metodo(6, 7) * parametro1
    }

    static funcionDeClase (parametro1, parametro2) {
        return parametro1 + parametro2
    }
}

// Así se instancia la "clase" (por atrás llama a la función `constructor`)
const miInstancia = new MiClase(2, 3)
// Así se llama uno de sus métodos
console.log(miInstancia.metodo2(4))
// Así se llama una de sus funciones de "clase"
console.log(MiClase(4, 5))
```
## ¿Cómo declarar una variable?

Hay dos maneras.

Si la variable no se va a reasignar, entonces usar la palabra-clave `const`.

```javascript
const miVariable = 6
```

En este caso, intentar asignar de nuevo a miVariable va a dar un error.
Cuidado que no es una constante exactamente. Se puede mutar. Ejemplo:

const miVariable = [1, 2, 3]
miVariable[0] = 0
expect(miVariable).toEqual([0, 2, 3])

Si se va a reasignar, entonces usar la palabra-clave `let`.

```javascript
let miVariable = 6
miVariable = 7
```

Nota: antes se usaba `var`. Por favor ya no usarlo.

## ¿Cómo crear una función?

Una función (no confundir con un método) se puede declarar de las siguientes maneras:

```javascript
function miFuncion(parametro1, parametro2) {
  return parametro1 + parametro2
}

const miFuncion = function(parametro1, parametro2) {
  return parametro1 + parametro2
}

const miFuncion = (parametro1, parametro2) => {
  return parametro1 + parametro2
}

const miFuncion = (parametro1, parametro2) => parametro1 + parametro2
```

Por favor favorecer las dos últimas formas, según si se puede escribir en una sentencia o no.

## ¿Necesito terminar las sentencias con un ";"?

En algunos casos es requerido, pero la tendencia actual es omitirles. Puede hacer según guste.
