import parImpar from '../src/parImpar'

describe('parImpar', () => {
  it('retorna "par" si el número es par', () => {
    parImpar()
  })

  it('retorna "impar" si el número es impar', () => {
  })

  it('retorna "tipo equivocado" cuando el argumento no es un número', () => {
  })
})
