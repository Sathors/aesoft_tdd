import escogerPareja from '../src/escogerPareja'

describe.skip('escogerPareja', () => {
  it('retorna un empleado si le damos una pareja', () => {
    expect(escogerPareja([1000, 2000])).toEqual([1000])
  })
})
