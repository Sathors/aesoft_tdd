import ModernArray from '../src/ModernArray'

describe.skip('ModernArray', () => {
  describe('includes', () => {
    it('retorna true si el elemento se encuentra en el arreglo', () => {
      const modernArray = new ModernArray([1, 2, 3, 4])
      expect(modernArray.includes(4)).toBe(true)
    })

    it('retorna false si el elemento no se encuentra en el arreglo', () => {
      const modernArray = new ModernArray([1, 2, 3, 4])
      expect(modernArray.includes(6)).toBe(false)
    })
  })

  describe('otro método', () => {
    it('caso 1', () => {
    })

    it('caso 2', () => {
    })
  })
})
