import WarpEngine from '../src/WarpEngine'

describe('WarpEngine', () => {
  describe('acceptance tests', () => {
    it.skip('checks case 1', () => {
      const warpEngine = new WarpEngine(0, 0, 0, 100)
      expect(warpEngine.optimumPlasmaFlow()).toEqual([100, 100, 100])
      expect(warpEngine.operatingTime()).toBe(Infinity)
    })

    it.skip('checks case 2', () => {
      const warpEngine = new WarpEngine(0, 0, 0, 90)
      expect(warpEngine.optimumPlasmaFlow()).toEqual([90, 90, 90])
      expect(warpEngine.operatingTime()).toBe(Infinity)
    })

    it.skip('checks case 3', () => {
      const warpEngine = new WarpEngine(0, 0, 0, 30)
      expect(warpEngine.optimumPlasmaFlow()).toEqual([30, 30, 30])
      expect(warpEngine.operatingTime()).toBe(Infinity)
    })

    it.skip('checks case 4', () => {
      const warpEngine = new WarpEngine(20, 10, 0, 100)
      expect(warpEngine.optimumPlasmaFlow()).toEqual([90, 100, 110])
      expect(warpEngine.operatingTime()).toBe(90)
    })

    it.skip('checks case 5', () => {
      const warpEngine = new WarpEngine(0, 0, 100, 80)
      expect(warpEngine.optimumPlasmaFlow()).toEqual([120, 120, 0])
      expect(warpEngine.operatingTime()).toBe(80)
    })

    it.skip('checks case 6', () => {
      const warpEngine = new WarpEngine(0, 0, 0, 150)
      expect(warpEngine.optimumPlasmaFlow()).toEqual([150, 150, 150])
      expect(warpEngine.operatingTime()).toBe(50)
    })

    it.skip('checks case 7', () => {
      const warpEngine = new WarpEngine(0, 0, 30, 140)
      expect(warpEngine.optimumPlasmaFlow()).toEqual([150, 150, 120])
      expect(warpEngine.operatingTime()).toBe(50)
    })

    it.skip('checks case 8', () => {
      const warpEngine = new WarpEngine(20, 50, 40, 170)
      expect(() => warpEngine.optimumPlasmaFlow()).toThrowError(Error,
        'Unable to comply')
      expect(warpEngine.operatingTime()).toBe(0)
    })
  })
})
