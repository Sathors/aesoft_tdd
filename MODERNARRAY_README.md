# ModernArray: una reimplementación de la "clase" Array

## Meta

Crear una "clase" ModernArray que hace lo mismo que la "clase" Array, la que está atrás cuando se crea un arreglo (por ejemplo [1, 3, 5] es una instancia de Array).

## Como se usa ModernArray

Los métodos de Array tienen que estar implementados como métodos de ModernArray, no como funciones.
Por ejemplo:

```javascript
const modernArray = new ModernArray([1, 2, 3, 4])
expect(modernArray.toString()).toBe('1, 2, 3, 4')
```

No queremos tener funciones en lugar de métodos. Lo siguiente está mal:

```javascript
expect(ModernArray.toString(1, 2, 3, 4)).toBe('1, 2, 3, 4')
```

## Los métodos que se tienen que implementar

En la implementación de cada método de ModernArray, no se puede llamar al método correspondiente de Array. Se tiene que reimplementar. Otros métodos de Array sí se pueden llamar si hace falta (mejor no).
Lo siguiente está prohibido:

```javascript
map (aFunction) {
    return this.array.map(aFunction)
}
```

Aquí está la lista de métodos que queremos implementar usando TDD, por favor en este mismo ordén.

- `indexOf`
- `lastIndexOf`
- `concat`
- `slice`
- `toString`
- `join`

- `forEach`
- `map`
- `filter`
- `reduce`
- `reduceRight`
- `every`
- `some`
- `find`
- `findIndex`

Pueden ver la documentación oficial de Array [aquí](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array) (o más completa en inglés [aquí](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array)).
