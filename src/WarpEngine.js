/*
 * The WarpEngine represents one of the models of the warp engine installed on
 * the Starship Enterprise.
 *
 * It has methods to calculate the necessary plasma flow to reach a certain
 * speed.
 */
export default class WarpEngine {
  constructor (damageToInjectorA, damageToInjectorB, damageToInjectorC,
    desiredSpeed) {
    console.log('In the constructor')
  }
}
