export default class ModernArray {
  constructor (arreglo) {
    this.arreglo = arreglo
  }

  includes (valor) {
    for (let elemento of this.arreglo) if (elemento === valor) return true
    return false
  }
}
